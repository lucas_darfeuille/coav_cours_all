//TOUT CES INCLUDES SONT IMPORTANTS !!!!!
#include <gcc-plugin.h>
#include <tree.h>
#include <context.h>
#include <gimple.h>
#include <tree-pass.h>
#include <basic-block.h>
#include <gimple-iterator.h>

int plugin_is_GPL_compatible;

/******************************/
/****   TD2 - QUESTION 7   ****/
/******************************/

/* Build a filename (as a string) based on function name */
        static char *
cfgviz_generate_filename( function * fun, const char * suffix )
{
        char * target_filename ;

        target_filename = (char *)xmalloc( 2048 * sizeof( char ) ) ;

        snprintf( target_filename, 1024, "%s_%s_%d_%s.dot",
                        current_function_name(),
                        LOCATION_FILE( fun->function_start_locus ),
                        LOCATION_LINE( fun->function_start_locus ),
                        suffix ) ;

        return target_filename ;
}

/* Dump the graphviz representation of function 'fun' in file 'out' */
        static void
cfgviz_internal_dump( function * fun, FILE * out )
{

        // Print the header line and open the main graph
                 fprintf(out, "Digraph G{\n");
        
        /*****************************/
        /***** COMPLETE HERE ********/
        basic_block bb;
        FOR_ALL_BB_FN(bb,cfun) {
        fprintf(out, "N%d [label=\"%s %d\" shape=ellipse]\n", bb->index,fndecl_name(cfun->decl), bb->index);
        edge e;
        edge_iterator ei;
        FOR_EACH_EDGE(e, ei, bb->succs) {
        fprintf(out, "N%d -> N%d\n", bb->index, e->dest->index);
        }}
         /*****************************/
        //close the main graph
        fprintf(out, "}\n");
        }

        void
cfgviz_dump( function * fun, const char * suffix )
{
        char * target_filename ;
        FILE * out ;

        target_filename = cfgviz_generate_filename( fun, suffix ) ;

        printf( "[GRAPHVIZ] Generating CFG of function %s in file <%s>\n",
                        current_function_name(), target_filename ) ;

        out = fopen( target_filename, "w" ) ;

        cfgviz_internal_dump( fun, out ) ;

        fclose( out ) ;
        free( target_filename ) ;
}

/******************************/
/**   TD2 - FIN QUESTION 7   **/
/******************************/


        
/*
enum pass_positioning_ops{
        PASS_POS_INSERT_AFTER, // Insert after the reference pass.
        PASS_POS_INSERT_BEFORE, // Insert before the reference pass.
        PASS_POS_REPLACE // Replace the reference pass.
};*/

/*
struct register_pass_info {
	opt_pass *pass; // New pass provided by the plugin.
	const char *reference_pass_name; // Name of the reference pass
for hooking up the new pass. 
	int ref_pass_instance_number; // Insert the pass at the specified
instance number of the reference pass. 
// Do it for every instance if it is 0.
	enum pass_positioning_ops pos_op; // how to insert the new pass.
};*/


const pass_data  my_pass_data={
GIMPLE_PASS,
"MY_PASS",
OPTGROUP_NONE,
TV_OPTIMIZE,
0,0,0,0,0,}; 

class my_pass : public gimple_opt_pass{
	public:
		my_pass (gcc::context *ctxt)
			: gimple_opt_pass (my_pass_data, ctxt){}

		my_pass *clone () { return new my_pass(g);}
		
		//MA FONCTION GATE	
		bool gate (function *fun) {
			printf("Dans le fonction gate\n");
			return true;}
		
		//MA FONCTION EXECUTE
		unsigned int execute (function *fun) {
			printf("Executing my_pass with function %s\n", function_name(fun));
			//printf("%s\n",fndecl_name(cfun->decl));
			basic_block bb;
			FOR_EACH_BB_FN(bb,cfun)
			{
			//rappel du ? : 
			//condition ? [truc si vrai] : [truc si faut];
			printf("Function: %s, Basic Block Index: %d\n",
               		fndecl_name(cfun->decl) ? fndecl_name(cfun->decl) : "<unknown>",
              		bb->index);
			gimple_stmt_iterator stmt = gsi_start_bb(bb);
			int flag = 1;
			while (flag) {

				if (is_gimple_call(gsi_stmt(stmt))){
					printf("C'est bien une fonction : %s\n", fndecl_name(cfun->decl));}

				printf("Line : %i\n", gimple_lineno(gsi_stmt(stmt)));
			  	flag = gsi_end_p(stmt);
				if (flag) {
					gsi_next(&stmt);
					 }
				}
			}
			
			    cfgviz_dump(fun, "CFG");

			return 0;}
};


int plugin_init(struct plugin_name_args * plugin_info,
		struct plugin_gcc_version * version) {

	struct register_pass_info pass_info;
	my_pass p(g);
	pass_info.pass = &p;
	pass_info.reference_pass_name = "cfg";
	pass_info.ref_pass_instance_number = 0;
	pass_info.pos_op = PASS_POS_INSERT_AFTER;

	register_callback(
		plugin_info->base_name,
		PLUGIN_PASS_MANAGER_SETUP,
		NULL,
		&pass_info);

	printf( "Entrée dans le premier plugin\n" );
	return 0;
}


