//TOUT CES INCLUDES SONT IMPORTANTS !!!!!
#include <gcc-plugin.h>
#include <tree.h>
#include <context.h>
#include <gimple.h>
#include <tree-pass.h>

int plugin_is_GPL_compatible;

/*
enum pass_positioning_ops{
        PASS_POS_INSERT_AFTER, // Insert after the reference pass.
        PASS_POS_INSERT_BEFORE, // Insert before the reference pass.
        PASS_POS_REPLACE // Replace the reference pass.
};*/

/*
struct register_pass_info {
	opt_pass *pass; // New pass provided by the plugin.
	const char *reference_pass_name; // Name of the reference pass
for hooking up the new pass. 
	int ref_pass_instance_number; // Insert the pass at the specified
instance number of the reference pass. 
// Do it for every instance if it is 0.
	enum pass_positioning_ops pos_op; // how to insert the new pass.
};*/


const pass_data  my_pass_data={
GIMPLE_PASS,
"MY_PASS",
OPTGROUP_NONE,
TV_OPTIMIZE,
0,0,0,0,0,}; 

class my_pass : public gimple_opt_pass{
	public:
		my_pass (gcc::context *ctxt)
			: gimple_opt_pass (my_pass_data, ctxt){}

		my_pass *clone () { return new my_pass(g);}
		
		//MA FONCTION GATE	
		bool gate (function *fun) {
			printf("Dans le fonction gate");
			return true;}
		
		//MA FONCTION EXECUTE
		unsigned int execute (function *fun) {
			printf("Executing my_pass with function %s\n", function_name(fun));
			return 0;}
};


int plugin_init(struct plugin_name_args * plugin_info,
		struct plugin_gcc_version * version) {

	struct register_pass_info pass_info;
	my_pass p(g);
	pass_info.pass = &p;
	pass_info.reference_pass_name = "cfg";
	pass_info.ref_pass_instance_number = 0;
	pass_info.pos_op = PASS_POS_INSERT_AFTER;

	register_callback(
		plugin_info->base_name,
		PLUGIN_PASS_MANAGER_SETUP,
		NULL,
		&pass_info);

	printf( "Entrée dans le premier plugin\n" );
	return 0;
}


