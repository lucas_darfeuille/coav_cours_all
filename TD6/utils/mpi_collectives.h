#pragma once

#include "plugin_headers.h"

#define DEFMPICOLLECTIVES(CODE, NAME, COLOR) CODE,
// The codes for the MPI collectives.
//
// `LAST_AND_UNUSED_MPI_COLLECTIVE_CODE` can be used to know the number
// of collectives.
enum mpi_collective_code {
#include "MPI_collectives.def"
    LAST_AND_UNUSED_MPI_COLLECTIVE_CODE,
};
#undef DEFMPICOLLECTIVES

#define DEFMPICOLLECTIVES(CODE, NAME, COLOR) NAME,
// The names of the MPI collectives. The index of the name is the same
// as the value behind its `enum mpi_collective_code` representation.
const char* const mpi_collective_names[] = {
#include "MPI_collectives.def"
};
#undef DEFMPICOLLECTIVES

#define DEFMPICOLLECTIVES(CODE, NAME, COLOR) COLOR,
// The colors of the MPI collectives. The index of the colors is the same
// as the value behind its `enum mpi_collective_code` representation.
//
// NOTE: unlike in `mpi_collective_names`, `LAST_AND_UNUSED_MPI_COLLECTIVE_CODE`
// has a color: black. This is done to facilitate graph files writing.
const char* const mpi_collective_colors[] = {
#include "MPI_collectives.def"
    "black",
};
#undef DEFMPICOLLECTIVES

// Get the MPI call corresponding to the given statement, if any.
//
// Will return `LAST_AND_UNUSED_MPI_COLLECTIVE_CODE` when no MPI call is found.
enum mpi_collective_code get_mpi_collective_stmt(const gimple* stmt)
{
    if (is_gimple_call(stmt)) {
        const tree t = gimple_call_fndecl(stmt);
        const char* ident = IDENTIFIER_POINTER(DECL_NAME(t));
        for (int i = 0; i < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE; i++) {
            if (strcmp(mpi_collective_names[i], ident) == 0) {
                return (enum mpi_collective_code)i;
            }
        }
    }

    return LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;
}

// Get the first MPI call in the passed basic block, if any.
//
// Will return `LAST_AND_UNUSED_MPI_COLLECTIVE_CODE` when no MPI call is found.
enum mpi_collective_code get_mpi_collective_bb(basic_block bb)
{
    // Iterates the statement in the basic block. If one is a collective call,
    // return its code.
    for (gimple_stmt_iterator gsi = gsi_start_bb(bb); !gsi_end_p(gsi); gsi_next(&gsi)) {
        const gimple* stmt = gsi_stmt(gsi);

        const enum mpi_collective_code code = get_mpi_collective_stmt(stmt);
        if (code != LAST_AND_UNUSED_MPI_COLLECTIVE_CODE) {
            return code;
        }
    }
    return LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;
}

// Split the basic block if it has more than one MPI collective.
//
// Return `true` if split, `false` if not.
bool split_bb_on_mpi_collectives(basic_block bb)
{
    int count = 0;
    for (gimple_stmt_iterator gsi = gsi_start_bb(bb); !gsi_end_p(gsi); gsi_next(&gsi)) {
        const gimple* stmt = gsi_stmt(gsi);

        if (is_gimple_call(stmt)) {
            const tree t = gimple_call_fndecl(stmt);
            const char* ident = IDENTIFIER_POINTER(DECL_NAME(t));

            for (int i = 0; i < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE; i++) {
                count += (strcmp(mpi_collective_names[i], ident) == 0);

                if (count >= 2) {
                    gsi_prev(&gsi);
                    gimple* prev_stmt = gsi_stmt(gsi);
                    split_block(bb, prev_stmt);
                    return true;
                }
            }
        }
    }

    return false;
}

// Split the basic blocks of the function if they contains at least two
// MPI collectives.
void split_on_mpi_collectives(function* fun)
{
    basic_block bb;
    FOR_EACH_BB_FN(bb, fun)
    {
        split_bb_on_mpi_collectives(bb);
    }
}
