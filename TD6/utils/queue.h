// This header provides a simple queue implementation.
//
// It exits because the standard library for C++ is not available when writing
// plugins for GCC 9.1.
//
// This is not AT ALL production-grade code, just a quick and dirty
// implementation for a school project.

#pragma once

#include <stdlib.h>

// A node in a queue, generic over the stored type.
//
// This uses the linked list approach to make for a very efficient FIFO in
// terms of insertion and deletion, though cache locality will take a hit.
template <typename T>
struct Node {
    Node(T val)
        : value(val)
        , next(nullptr)
    {
    }

    // The value to store in the node.
    T value;
    // Next node in the queue. A `nullptr` value means the current node is
    // at the end of its queue.
    Node* next;
};

// A basic FIFO implementation.
//
// Use `push` to append elements at the end and `pop` to remove them at the
// front. `is_empty` can be used to check the state of the queue.
//
// `push` and `pop` should be O(1) barring (de)allocations.
template <typename T>
class Queue {
public:
    // Builds an empty queue (does not allocate).
    Queue()
        : head(nullptr)
        , rear(nullptr)
    {
    }

    // Push the given value at the end of the queue.
    //
    // O(1), barring allocation of the node.
    void push(T val)
    {
        Node<T>* new_node = new Node<T>(val);

        // If the queue was empty before, put the new node at the front of
        // the queue else append it to the end and update the rear pointer.
        if (this->head == nullptr) {
            this->head = new_node;
            this->rear = this->head;
        } else {
            this->rear->next = new_node;
            this->rear = this->rear->next;
        }
    }

    // Pop the element at the front and return it.
    //
    // This will abort the current process if the queue is empty.
    //
    // O(1), barring deallocation of the node.
    T pop()
    {
        // Sanity check.
        if (this->head == nullptr) {
            exit(1);
        }

        // Getting the value and preparing to deallocate the current head.
        T value = this->head->value;
        Node<T>* del_head = this->head;

        // Moving the head, updating the rear if necessary.
        this->head = this->head->next;
        delete del_head;
        if (this->head == nullptr) {
            this->rear = nullptr;
        }

        return value;
    }

    // Return `true` if the queue is empty.
    bool is_empty() const
    {
        return this->head == nullptr;
    }

private:
    // Head of the queue, allows for O(1) deletion.
    //
    // A `nullptr` value means the queue is empty.
    Node<T>* head;
    // End of the queue, allows for O(1) insertion.
    Node<T>* rear;
};
