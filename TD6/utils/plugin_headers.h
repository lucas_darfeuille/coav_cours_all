#pragma once

#include <gcc-plugin.h>

#include <plugin-version.h>

#include <tree.h>

#include <vec.h>

#include <coretypes.h>

#include <basic-block.h>

#include <gimple.h>

#include <tree-pass.h>

#include <context.h>

#include <function.h>

#include <gimple-iterator.h>

#include <dominance.h>

#include <bitmap.h>

#include <c-family/c-pragma.h>

#include <errors.h>
