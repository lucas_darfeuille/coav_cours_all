#include <string.h>
#include <stdio.h>
#include "utils/plugin_headers.h"
#include "utils/cfgviz.h"
#include "utils/mpi_collectives.h"

/*********************
 * 
 * Gestion de la PASS
 * 
**********************/
const pass_data my_pass_data = {
    .type = GIMPLE_PASS,
    .name = "TD6 Q5",
    .optinfo_flags = OPTGROUP_NONE,
    .tv_id = TV_OPTIMIZE,
    .properties_required = 0,
    .properties_provided = 0,
    .properties_destroyed = 0,
    .todo_flags_start = 0,
    .todo_flags_finish = 0,
};

class my_pass : public gimple_opt_pass {
public:
    my_pass(gcc::context* ctxt)
        : gimple_opt_pass(my_pass_data, ctxt)
    {
    }

    my_pass* clone() { return new my_pass(g); }

    bool gate(function*) { return true; }

    unsigned int execute(function* fun)
    {
        printf("Function: '%s'\n", function_name(fun));

        return 0;
    }
};

/*********************
 * 
 * Gestion des PRAGMA
 * 
**********************/

// Vecteur qui servira pour la liste des fonctions instruments
vec<const unsigned char*> pragma_instrumented_functions {};

/******************
 * 
 * TD6 - Question 3
 * 
*******************/
// Ceci est la fonction instrument 
// Elle est appelée lors de la reconnaissance d'un pragma (cf ci-dessous)
// On s'inspire de ce qu'il se fait dans la fonction handle_pragma_target
void pragma_instrument_function(cpp_reader* ARG_UNUSED(dummy)) {
    /*
    * Début question 4
    */
    // Gestion d'erreur, si on est dans une fonction, on le signale
    // Car on ne met pas de pragma instrument dans nos fonctions
    if (cfun) {
        error("#pragma instrument option is not allowed inside functions");
        return;
    }
    /*
     * Fin question 4
    */

    // On récupère les informations utiles
    // Le booléen est là pour vérifier si le pragma est bien formé (vis à vis des parenthèses)
    bool closed_parenthese = false;
    tree x;
    enum cpp_ttype token;

    //pragma_lex permet d'avoir le prochain "mot" du pragma
    // Donc on commence par regarder si on a une parenthèse ouvrante
    token = pragma_lex(&x);
    if (CPP_OPEN_PAREN == token) {
        // il faudra refermer la parenthèse
        closed_parenthese = true;
        // On passe à l'élément suivant pour la suite du traitement
        token = pragma_lex(&x);
    }

    /*
    * Début question 6 partie 1
    */
    // On créé un vecteur dans lequel on va ajouter les fonctions à mettre
    // A la fin, si le pragma est bien formé, on ajoutera le tout à la variable globale
    // Sinon, on ne fait rien
    vec<const unsigned char*> functions_to_add {};

    //On définit la variable définira si on doit ajouter ou pas
    bool add_instrumented_to_global_variable = false;
    // Ceci nous permet d'avancer dans le traitement des éléments du pragma
    // On change quand on reconnait un CPP_COMMA (une virgule)
    // On traite alors bien les #pragma instrument function (f1,f2,f3...)
    while (CPP_NAME == token) {
        
        //On commence par récupérer le nom de notre fonction
        const unsigned char* name = x->identifier.id.str;
        // Puis on la met dans la liste de nos fonctions instrumentées
        functions_to_add.safe_push(name);
        /*
        * Fin question 6 partie 1
        */

        do {
            token = pragma_lex(&x);
        } while (CPP_COMMA == token);
    }

    /*
    * Début question 6 partie 2
    */
    // Quand on arrive ici, on a finit les fonctions, donc il faut clore la parenthèse ouvrante
    // C'est ce qu'on vérifie ici
    if (closed_parenthese && CPP_CLOSE_PAREN != token) {
        //Si on est ici, c'est que notre pragma est malformé, donc on ne doit pas ajouter les fonctions
        add_instrumented_to_global_variable = false;
        error("%<#pragma instrument (name1[, name2]...)%> is missing a closing %<)%>");
        return;
    }

    // Gestion d'erreur
    // Si on continue de reconnaitre des éléments après la fin théorique du pragma
    // Alors il faut prévenir l'utilisateur que ce dernier est mal écrit
    token = pragma_lex(&x);
    if (CPP_EOF != token) {
        //Si on est ici, c'est que notre pragma est malformé, donc on ne doit pas ajouter les fonctions
        add_instrumented_to_global_variable = false;
        error("%<#pragma instrument ...%> is badly formed");
        return;
    }
    /*
    * Fin question 6 partie 2
    */

    /*
    * Début question 6 partie 3
    */
    // On vérifie si on doit ajouter les fonctions instrumentés
    // Le cas échéant, on ajoute le tout à la variable globale
    unsigned int i;
    if (add_instrumented_to_global_variable) {
        // On prend chaque élément du vecteur et on le push dans la var globale
        FOR_EACH_VEC_ELT(functions_to_add, i, fun_name) {
            // On vérifie que la fonction à ajouter n'est pas dans la var globale
            // Si elle dedans, on fait un warning et on ne l'ajoute pas
            unsigned int j;
            int is_present = 0;
            FOR_EACH_VEC_ELT(pragma_instrument_function, j, name) {
                if (strcmp(name, fun_name) == 0) {
                // Si on arrive là, c'est qu'on a trouvé une correspondance
                is_present +=1;
                // J'hésite à mettre un break ici, mais j'ai peur donc je met rien x)
                // Mais dans l'idée on pourrait arrêter les tours de boucle ici
                }
            }
            if (is_present != 0) {
                //Emission du warning
                warning("Function '%s' is already in the instrumented functions list", fun_name);
            } 
            else {
            pragma_instrumented_functions.safe_push(fun_name);
            }
        }
    }
    /*
    * Fin question 6 partie 3
    */
}

// Fonction permettant d'enregistrer nos pragma customs
// C'est c_register_pragma qui fait la travail ici. 
// Quand est reconnu un #pragma instrument function, alors on appelle pragma_instrument_function
void register_my_pragma(void *event_data, void *data)
{
    c_register_pragma("instrument", "function", pragma_instrument_function);
    printf("Nouveau pragma !\n");
}

int plugin_is_GPL_compatible = 1;

int plugin_init(struct plugin_name_args* plugin_info,
    struct plugin_gcc_version* version)
{
    if (!plugin_default_version_check(version, &gcc_version))
        return 1;

    register_callback(plugin_info->base_name, PLUGIN_PRAGMAS, register_my_pragma, NULL);

    return 0;
}