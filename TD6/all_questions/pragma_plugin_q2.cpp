#include <string.h>
#include <stdio.h>
#include "utils/plugin_headers.h"
#include "utils/cfgviz.h"
#include "utils/mpi_collectives.h"


/******************
 * 
 * TD6 - Question 2
 * 
*******************/
// Ceci est la fonction instrument 
// Elle est appelée lors de la reconnaissance d'un pragma (cf ci-dessous)
// On s'inspire de ce qu'il se fait dans la fonction handle_pragma_target
void pragma_instrument_function(cpp_reader*) { 
    // On récupère l'information utile 
    enum cpp_ttype token;
    tree x;
    //pragma_lex permet d'avoir le prochain "mot" du pragma
    token = pragma_lex(&x);
    if (token == CPP_NAME) {
        printf("Function found is named : '%s'\n", x->identifier.id.str);
    }
}

// Fonction permettant d'enregistrer nos pragma customs
// C'est c_register_pragma qui fait la travail ici. 
// Quand est reconnu un #pragma instrument function, alors on appelle pragma_instrument_function
void register_my_pragma(void *event_data, void *data)
{
    c_register_pragma("instrument", "function", pragma_instrument_function);
    printf("Nouveau pragma !\n");
}

int plugin_is_GPL_compatible = 1;

int plugin_init(struct plugin_name_args* plugin_info,
    struct plugin_gcc_version* version)
{
    if (!plugin_default_version_check(version, &gcc_version))
        return 1;

    register_callback(plugin_info->base_name, PLUGIN_PRAGMAS, register_my_pragma, NULL);

    return 0;
}