#include <string.h>
#include <stdio.h>
#include "utils/plugin_headers.h"
#include "utils/cfgviz.h"
#include "utils/mpi_collectives.h"

/*********************
 * 
 * Gestion de la PASS
 * 
**********************/
const pass_data my_pass_data = {
    .type = GIMPLE_PASS,
    .name = "TD6 Q5",
    .optinfo_flags = OPTGROUP_NONE,
    .tv_id = TV_OPTIMIZE,
    .properties_required = 0,
    .properties_provided = 0,
    .properties_destroyed = 0,
    .todo_flags_start = 0,
    .todo_flags_finish = 0,
};

class my_pass : public gimple_opt_pass {
public:
    my_pass(gcc::context* ctxt)
        : gimple_opt_pass(my_pass_data, ctxt)
    {
    }

    my_pass* clone() { return new my_pass(g); }

    bool gate(function*) { return true; }

    unsigned int execute(function* fun)
    {
        printf("Function: '%s'\n", function_name(fun));

        return 0;
    }
};

/*********************
 * 
 * Gestion des PRAGMA
 * 
**********************/

// Vecteur qui servira pour la liste des fonctions instruments
vec<const unsigned char*> pragma_instrumented_functions {};

/******************
 * 
 * TD6 - Question 3
 * 
*******************/
// Ceci est la fonction instrument 
// Elle est appelée lors de la reconnaissance d'un pragma (cf ci-dessous)
// On s'inspire de ce qu'il se fait dans la fonction handle_pragma_target
void pragma_instrument_function(cpp_reader* ARG_UNUSED(dummy)) {
    /*
    * Début question 4
    */
    // Gestion d'erreur, si on est dans une fonction, on le signale
    // Car on ne met pas de pragma instrument dans nos fonctions
    if (cfun) {
        error("#pragma instrumente option is not allowed inside functions");
        return;
    }
    /*
     * Fin question 4
    */

    // On récupère les informations utiles
    // Le booléen est là pour vérifier si le pragma est bien formé (vis à vis des parenthèses)
    bool closed_parenthese = false;
    tree x;
    enum cpp_ttype token;

    //pragma_lex permet d'avoir le prochain "mot" du pragma
    // Donc on commence par regarder si on a une parenthèse ouvrante
    token = pragma_lex(&x);
    if (CPP_OPEN_PAREN == token) {
        // il faudra refermer la parenthèse
        closed_parenthese = true;
        // On passe à l'élément suivant pour la suite du traitement
        token = pragma_lex(&x);
    }

    // Ceci nous permet d'avancer dans le traitement des éléments du pragma
    // On change quand on reconnait un CPP_COMMA (une virgule)
    // On traite alors bien les #pragma instrument function (f1,f2,f3...)
    while (CPP_NAME == token) {
        /*
        * Début question 5
        */
        //On commence par récupérer le nom de notre fonction
        const unsigned char* name = x->identifier.id.str;
        // Puis on la met dans la liste de nos fonctions instrumentées
        pragma_instrumented_functions.safe_push(name);
        /*
        * Fin question 5
        */
        printf("Function found is named : '%s'\n", x->identifier.id.str);

        do {
            token = pragma_lex(&x);
        } while (CPP_COMMA == token);
    }

    // Quand on arrive ici, on a finit les fonctions, donc il faut clore la parenthèse ouvrante
    // C'est ce qu'on vérifie ici
    if (closed_parenthese && CPP_CLOSE_PAREN != token) {
        error("%<#pragma instrument (name1[, name2]...)%> is missing a closing %<)%>");
        return;
    }

    // Gestion d'erreur
    // Si on continue de reconnaitre des éléments après la fin théorique du pragma
    // Alors il faut prévenir l'utilisateur que ce dernier est mal écrit
    token = pragma_lex(&x);
    if (CPP_EOF != token) {
        error("%<#pragma instrumente ...%> is badly formed");
        return;
    }

    // Ceci est à but de test
    // À décommenter pour print ce qu'on a dans le vecteur 
    // Contenant la liste des fonctions instrumentées

    // unsigned int i;
    // const unsigned char* function_name;
    // printf("Instrumented functions: ");
    // FOR_EACH_VEC_ELT(pragma_instrumented_functions, i, function_name)
    // {
    //     printf("%s ", function_name);
    // }

}

// Fonction permettant d'enregistrer nos pragma customs
// C'est c_register_pragma qui fait la travail ici. 
// Quand est reconnu un #pragma instrument function, alors on appelle pragma_instrument_function
void register_my_pragma(void *event_data, void *data)
{
    c_register_pragma("instrument", "function", pragma_instrument_function);
    printf("Nouveau pragma !\n");
}

int plugin_is_GPL_compatible = 1;

int plugin_init(struct plugin_name_args* plugin_info,
    struct plugin_gcc_version* version)
{
    if (!plugin_default_version_check(version, &gcc_version))
        return 1;

    register_callback(plugin_info->base_name, PLUGIN_PRAGMAS, register_my_pragma, NULL);

    return 0;
}