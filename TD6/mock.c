/**
 * \file main.c
 * \brief TEST PLUGIN
*/
#include <stdio.h>
#include "mock.h"

#pragma instrument function mock1
#pragma instrument function mock2
#pragma instrument function mock3,mock5
#pragma instrument function (mock4,mock6)
#pragma instrument function CANARD
#pragma instrument function mock1
#pragma instrument function (coincoin

void mock1() {
	printf("mock1\n");
}

void mock2() {
	printf("mock2\n");
}

void mock3() {
	printf("mock3\n");
}

void mock4() {
	printf("mock4\n");
}
