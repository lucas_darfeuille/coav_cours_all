#include <gcc-plugin.h>
#include <plugin-version.h>
#include <tree.h>
#include <basic-block.h>
#include <gimple.h>
#include <tree-pass.h>
#include <context.h>
#include <function.h>
#include <gimple-iterator.h>


/* Global variable required for plugin to execute */
int plugin_is_GPL_compatible;


/* Global object (const) to represent my pass */
const pass_data my_pass_data =
{
	GIMPLE_PASS, /* type */
	"NEW_PASS", /* name */
	OPTGROUP_NONE, /* optinfo_flags */
	TV_OPTIMIZE, /* tv_id */
	0, /* properties_required */
	0, /* properties_provided */
	0, /* properties_destroyed */
	0, /* todo_flags_start */
	0, /* todo_flags_finish */
};                                                                                                                                     




/* Enum to represent the collective operations */
enum mpi_collective_code {
#define DEFMPICOLLECTIVES( CODE, NAME ) CODE,
#include "MPI_collectives.def"
	LAST_AND_UNUSED_MPI_COLLECTIVE_CODE
#undef DEFMPICOLLECTIVES
} ;

/* Name of each MPI collective operations */
#define DEFMPICOLLECTIVES( CODE, NAME ) NAME,
const char *const mpi_collective_name[] = {
#include "MPI_collectives.def"
} ;
#undef DEFMPICOLLECTIVES

// Structure nécessaire pour la question 5 du TD3
struct mpi_block_info {
    mpi_collective_code mpi_function;
};


void td_isol_print(int td)
{
	printf("\n\n\n");
	printf("/****************************************************************************************************/\n");
	printf("/****************************************************************************************************/\n");
	printf("/*************************                       TD%d                        *************************/\n", td);
	printf("/****************************************************************************************************/\n");
	printf("/****************************************************************************************************/\n");
	printf("\n\n\n");


}


void function_isol_print(function *fun)
{
	printf("\n\n\n");
	printf("/************************************************************************************************************************/\n");
	printf("/************************************************************************************************************************/\n");
	printf("/***********************************            %s               ***********************************/\n", function_name(fun) );
	printf("/************************************************************************************************************************/\n");
	printf("/************************************************************************************************************************/\n");
	printf("\n\n\n");


}


/****************************************************************************************************/
/****************************************************************************************************/
/*************************                       TD2                        *************************/
/****************************************************************************************************/
/****************************************************************************************************/




// /******************************/
// /**** TD2 - QUESTION 3 & 4 ****/
// /******************************/

// const char * td2_q3_q4_print_func_name(function * fun)
// {
// 	const char * fname = function_name(fun);
// 	printf("\t ... in function %s\n", fname);

// 	return fname;
// }

// /******************************/
// /** TD2 - FIN QUESTION 3 & 4 **/
// /******************************/


// /******************************/
// /****   TD2 - QUESTION 8   ****/
// /******************************/

// void td2_q8_print_called_functions( basic_block bb )
// {
// 	gimple_stmt_iterator gsi;

// 	// /* Iterate on gimple statements in the current basic block */
// 	// for (gsi = gsi_start_bb (bb); !gsi_end_p (gsi); gsi_next (&gsi))
// 	// {
// 	// 	/* Get the current statement */
// 	// 	gimple *stmt = gsi_stmt (gsi);

// 	// 	if (is_gimple_call (stmt))
// 	// 	{
// 	// 		tree t ;
// 	// 		const char * callee_name ;

// 	// 		t = gimple_call_fndecl( stmt ) ;
// 	// 		callee_name = IDENTIFIER_POINTER(DECL_NAME(t)) ;


// 	// 		printf("          |||++|||      - gimple statement is a function call: function called is \" %s \" \n", callee_name);


// 	// 	}
// 	// }
// 	for (gsi = gsi_start_bb(bb); !gsi_end_p(gsi); gsi_next(&gsi)) {
//         /* Get the current statement */
//         gimple *stmt = gsi_stmt(gsi);

//         if (is_gimple_call(stmt)) {
//             print_mpi_function_name(stmt);
//         }
//     }
// }
// /******************************/
// /**   TD2 - FIN QUESTION 8   **/
// /******************************/




// /******************************/
// /**** TD2 - QUESTION 5 & 6 ****/
// /******************************/

// void td2_q5_q6_print_blocks(function * fun)
// {
// 	basic_block bb;
// 	gimple_stmt_iterator gsi;
// 	gimple *stmt;

// 	FOR_EACH_BB_FN(bb,fun)
// 	{
// 		gsi = gsi_start_bb(bb);
// 		stmt = gsi_stmt(gsi);
// 		printf("          |||++||| BLOCK INDEX %d : LINE %d\n", bb->index, gimple_lineno(stmt));

// 		td2_q8_print_called_functions(bb);

// 	}
// }

// /******************************/
// /** TD2 - FIN QUESTION 5 & 6 **/
// /******************************/





/****************************************************************************************************/
/****************************************************************************************************/
/*************************                     FIN TD2                      *************************/
/****************************************************************************************************/
/****************************************************************************************************/

/****************************************************************************************************/
/****************************************************************************************************/
/*************************                       TD3                        *************************/
/****************************************************************************************************/
/****************************************************************************************************/

bool is_mpi_function(const char *function_name) {
	//This is for Q2
    for (int i = 0; i < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE; ++i) {
        if (strcmp(function_name, mpi_collective_name[i]) == 0) {
            return true;
        }
    }
    return false;
	
	//This is for Q1
	//return strncmp(function_name, "MPI_", 4) == 0;
}

mpi_collective_code is_mpi_call(gimple *stmt) {
    if(is_gimple_call(stmt)){
        const char* callee_name = IDENTIFIER_POINTER(DECL_NAME(gimple_call_fndecl(stmt)));
        for(int code = 0; code < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE; code++){
            if(strcmp((char*)callee_name, mpi_collective_name[code]) == 0){
                return (mpi_collective_code) code;
            }
        }
    }
    return LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;
}



// // This is for Q2
// // Function to print the MPI function name if the statement is a function call 
// void print_mpi_function_name(gimple *stmt) {
//     if (is_gimple_call(stmt)) {
//         tree t = gimple_call_fndecl(stmt);
//         const char *callee_name = IDENTIFIER_POINTER(DECL_NAME(t));

//         if (is_mpi_function(callee_name)) {
//             printf("MPI Function Call: %s\n", callee_name);
//         }
//     }
//}


// This is for Q3
mpi_collective_code get_mpi_function_enum(const char *function_name) {
    for (int i = 0; i < LAST_AND_UNUSED_MPI_COLLECTIVE_CODE; ++i) {
        if (strcmp(function_name, mpi_collective_name[i]) == 0) {
            return static_cast<mpi_collective_code>(i);
        }
    }
    return LAST_AND_UNUSED_MPI_COLLECTIVE_CODE;
}


// This is for Q5
// Function to print the MPI function name and its enum value if the statement is a function call
void print_mpi_function_name(gimple *stmt, basic_block bb) {
    if (is_gimple_call(stmt)) {
        tree t = gimple_call_fndecl(stmt);
        const char *callee_name = IDENTIFIER_POINTER(DECL_NAME(t));

        mpi_collective_code mpi_enum = get_mpi_function_enum(callee_name);

        if (mpi_enum != LAST_AND_UNUSED_MPI_COLLECTIVE_CODE) {
            printf("MPI Function Call: %s, Enum Value: %d\n", callee_name, mpi_enum);

            /* Create and store mpi_block_info in the basic block's aux field */
            mpi_block_info *block_info = (mpi_block_info *)xcalloc(1, sizeof(mpi_block_info));
            block_info->mpi_function = mpi_enum;
            bb->aux = (PTR)block_info;
        }
    }
}



//And the modified print_mpi_function_name :
// void print_mpi_function_name(gimple *stmt) {
//     if (is_gimple_call(stmt)) {
//         tree t = gimple_call_fndecl(stmt);
//         const char *callee_name = IDENTIFIER_POINTER(DECL_NAME(t));

//         mpi_collective_code mpi_enum = get_mpi_function_enum(callee_name);

//         if (mpi_enum != LAST_AND_UNUSED_MPI_COLLECTIVE_CODE) {
//             printf("MPI Function Call: %s, Enum Value: %d\n", callee_name, mpi_enum);
//         }
//     }
// }

// This is also for Q5
// Function to print called MPI functions and associated basic block information
void print_mpi_functions_and_blocks(function *fun) {
    basic_block bb;
    gimple_stmt_iterator gsi;
    gimple *stmt;

    FOR_EACH_BB_FN(bb, fun) {
        gsi = gsi_start_bb(bb);
        stmt = gsi_stmt(gsi);

        /* Print the MPI function name and enum value */
        print_mpi_function_name(stmt, bb);

        /* Retrieve mpi_block_info from the basic block's aux field */
        mpi_block_info *block_info = (mpi_block_info *)bb->aux;
        if (block_info != NULL) {
            printf("Basic Block %d: Associated MPI Function Enum: %d\n", bb->index, block_info->mpi_function);
        }
    }
}

// Function to reset the aux field of each basic block
void reset_aux_fields(function *fun) {
    basic_block bb;
    FOR_EACH_BB_FN(bb, fun) {
        bb->aux = NULL;
    }
}

// This is for Q7
// Function to check if there is at least one basic block with at least two recognized MPI calls
bool has_multiple_mpi_calls(function *fun) {
    basic_block bb;
    gimple_stmt_iterator gsi;
    gimple *stmt;

    FOR_EACH_BB_FN(bb, fun) {
        int mpi_call_count = 0;

        for (gsi = gsi_start_bb(bb); !gsi_end_p(gsi); gsi_next(&gsi)) {
            stmt = gsi_stmt(gsi);

            if (is_gimple_call(stmt)) {
                tree t = gimple_call_fndecl(stmt);
                const char *callee_name = IDENTIFIER_POINTER(DECL_NAME(t));

                mpi_collective_code mpi_enum = get_mpi_function_enum(callee_name);

                if (mpi_enum != LAST_AND_UNUSED_MPI_COLLECTIVE_CODE) {
                    // Recognized MPI call
                    mpi_call_count++;

                    if (mpi_call_count >= 2) {
                        // At least two recognized MPI calls in the same basic block
                        return true;
                    }
                }
            }
        }
    }

    // No basic block with at least two recognized MPI calls
    return false;
}

//This is for Q8
void split_blocks(function *fun)
{
    basic_block bb;
    gimple_stmt_iterator gsi;
    gimple *stmt;
    size_t nb_collective;

    FOR_EACH_BB_FN(bb, fun)
    {
        nb_collective = 0;
        for (gsi = gsi_start_bb(bb); !gsi_end_p(gsi); gsi_next(&gsi))
        {
            stmt = gsi_stmt(gsi);
            if (is_mpi_function(stmt) != LAST_AND_UNUSED_MPI_COLLECTIVE_CODE)
                ++nb_collective;
            if (nb_collective >= 2)
                split_block(bb, stmt);
        }
    }
}

/****************************************************************************************************/
/****************************************************************************************************/
/*************************                     FIN TD3                      *************************/
/****************************************************************************************************/
/****************************************************************************************************/

/****************************************************************************************************/
/****************************************************************************************************/
/*************************                PLUGIN GRAPHVIZ                   *************************/
/****************************************************************************************************/
/****************************************************************************************************/




/* Build a filename (as a string) based on function name */
static char * cfgviz_generate_filename( function * fun, const char * suffix )
{
	char * target_filename ;

	target_filename = (char *)xmalloc( 1024 * sizeof( char ) ) ;

	snprintf( target_filename, 1024, "%s_%s_%d_%s.dot",
			current_function_name(),
			LOCATION_FILE( fun->function_start_locus ),
			LOCATION_LINE( fun->function_start_locus ),
			suffix ) ;

	return target_filename ;
}

// Modified version of the function for Q6
/* Dump the graphviz representation of function 'fun' in file 'out' */
static void cfgviz_internal_dump(function *fun, FILE *out, int td) {
    basic_block bb;

    // Print the header line and open the main graph
    fprintf(out, "Digraph G{\n");

    /******************************/
    /****   TD2 - QUESTION 7   ****/
    /******************************/

    FOR_ALL_BB_FN(bb, cfun) {

        //
        // Print the basic block BB, with the MPI call if necessary
        //

        fprintf(out,
                "%d [label=\"BB %d\\n",
                bb->index,
                bb->index
        );

        // Process the MPI function in the basic block
		mpi_block_info *block_info = (mpi_block_info *)bb->aux;
		if (block_info != NULL) {
    		mpi_collective_code mpi_enum = block_info->mpi_function;
    		if (mpi_enum != LAST_AND_UNUSED_MPI_COLLECTIVE_CODE) {
        		fprintf(out, "MPI: %s\\n", mpi_collective_name[mpi_enum]);
    		}
		}


        fprintf(out, "\" shape=ellipse]\n");

        //
        // Process output edges
        //
        edge_iterator eit;
        edge e;

        FOR_EACH_EDGE(e, eit, bb->succs) {
            const char *label = "";
            if (e->flags == EDGE_TRUE_VALUE)
                label = "true";
            else if (e->flags == EDGE_FALSE_VALUE)
                label = "false";

            fprintf(out, "%d -> %d [color=red label=\"%s\"]\n",
                    bb->index, e->dest->index, label);
        }
    }
    /******************************/
    /**   TD2 - FIN QUESTION 7   **/
    /******************************/

    // Close the main graph
    fprintf(out, "}\n");
}


void
cfgviz_dump( function * fun, const char * suffix, int td )
{
	char * target_filename ;
	FILE * out ;

	target_filename = cfgviz_generate_filename( fun, suffix ) ;

	printf( "[GRAPHVIZ] Generating CFG of function %s in file <%s>\n",
			current_function_name(), target_filename ) ;

	out = fopen( target_filename, "w" ) ;

	cfgviz_internal_dump( fun, out, td ) ;

	fclose( out ) ;
	free( target_filename ) ;
}


/****************************************************************************************************/
/****************************************************************************************************/
/*************************            FIN  PLUGIN GRAPHVIZ                 *************************/
/****************************************************************************************************/
/****************************************************************************************************/



// void td2_through_the_cfg(function * fun)
// {
// 	td2_q3_q4_print_func_name(fun);
// 	td2_q5_q6_print_blocks(fun);
// }



/* My new pass inheriting from regular gimple pass */
class my_pass : public gimple_opt_pass
{
	public:
		my_pass (gcc::context *ctxt)
			: gimple_opt_pass (my_pass_data, ctxt)
		{}

		/* opt_pass methods: */

		my_pass *clone ()
		{
			return new my_pass(g);
		}

		/* Gate function (shall we apply this pass?) */
		bool gate (function *fun)
		{
			function_isol_print(fun);
			printf("plugin: gate... \n");
			//td2_q3_q4_print_func_name(fun);
			return true;
		}

		/* Execute function */
		unsigned int execute (function *fun)
		{
			printf("plugin: execute...\n");

			//td_isol_print(/*TD*/2);

			/******************************/
			/**** TD2 - QUESTION 3 à 8 ****/
			/******************************/

			//td2_through_the_cfg(fun);

			/******************************/
			/** TD2 - FIN QUESTION 3 à 8 **/
			/******************************/


			/******************************/
			/****   TD2 - QUESTION 7   ****/
			/******************************/

			/* Skip system header functions */
			//if ( !in_system_header_at( fun->function_start_locus ) )
				//cfgviz_dump( fun, "0_ini", /*TD*/2 ) ;

			/******************************/
			/**   TD2 - FIN QUESTION 7   **/
			/******************************/



			return 0;
		}


};



/* Main entry point for plugin */
int
plugin_init(struct plugin_name_args * plugin_info,
		struct plugin_gcc_version * version)
{
	struct register_pass_info my_pass_info;

	printf( "plugin_init: Entering...\n" ) ;

	/* First check that the current version of GCC is the right one */

	if(!plugin_default_version_check(version, &gcc_version))
		return 1;

	printf( "plugin_init: Check ok...\n" ) ;

	/* Declare and build my new pass */
	my_pass p(g);

	/* Fill info on my pass 
	   (insertion after the pass building the CFG) */
	my_pass_info.pass = &p;
	my_pass_info.reference_pass_name = "cfg";
	my_pass_info.ref_pass_instance_number = 0;
	my_pass_info.pos_op = PASS_POS_INSERT_AFTER;

	/* Add my pass to the pass manager */
	register_callback(plugin_info->base_name,
			PLUGIN_PASS_MANAGER_SETUP,
			NULL,
			&my_pass_info);

	printf( "plugin_init: Pass added...\n" ) ;

	return 0;
}


