# Plugin GCC pour la detection de deadlock MPI à la compilation et création de pragma personnalisés

Ceci est un projet d'étude de Lucas Darfeuille et Aurélien Coreau dans le cadre d'un cours de compilation avancée. Nous y abordons nos différents tds, ainsi que le projet de création d'un plugin GCC. Nous abordons aussi la création de pragma personnalisés.

# Dépendances

Le plugin est prévu pour fonctionner avec gcc 12.2. Avec quelques tests, il semble aussi fonctionner pour un certain nombre de version antérieures. 
Cela semble donc être bon pour gcc 8.4 et plus. Cependant, aucune garantie n'est fournie pour ces versions. Le plugin est sensé être utilisé avec GCC 12.2 uniquement.

# Pour utiliser le plugin

En l'état, ceci est un repos contenant l'intégralité du cours (le projet est dessus aussi du coup). Afin de l'utiliser je vous invite à modifier légèrement le Makefile en fonction de votre installation de gcc personnelle, et des alias présents.
Si vous êtes sur le cluster, pensez à faire un :
`source /home/patrick.carribault/setenv.sh`

Quand vous avez fait ceci (et/ou modifié les alias du makefile) vous pouvez : 
`cd <path-to-this-project>/rendu_final`
`make`
Afin de construire le plugin.

Pour lancer nos tests vous pouvez :
Run all tests : `make test_all`
Run les tests relatifs aux pragmas : `make pragma`
Run les tests relatifs aux deadlocks : `make deadlocks`

`make clean`supprime tous les .o ainsi que le binaire du plugin.