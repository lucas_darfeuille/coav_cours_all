#pragma once

#include <stdio.h>
#include <stdlib.h>

#include "plugin_headers.h"
#include "mpi_collectives.h"

// Build a filename (as a string) based on function name
static char* cfgviz_generate_filename(
    const function* const fun, const char* const suffix)
{
    char* target_filename;

    target_filename = (char*)xmalloc(2048 * sizeof(char));

    snprintf(target_filename,
        1024,
        "%s_%s_%d_%s.dot",
        current_function_name(),
        LOCATION_FILE(fun->function_start_locus),
        LOCATION_LINE(fun->function_start_locus),
        suffix);

    return target_filename;
}

// Dump the graphviz representation of function 'fun' in file 'out'
static void cfgviz_internal_dump(const function* const fun, FILE* out)
{
    // Print the header line and open the main graph
    fprintf(out, "Digraph G {\n");

    basic_block bb;
    FOR_EACH_BB_FN(bb, fun)
    {
        const enum mpi_collective_code code = get_mpi_collective_bb(bb);

        const char* coll_text;
        const char* style;
        if (code == LAST_AND_UNUSED_MPI_COLLECTIVE_CODE) {
            coll_text = "No collective";
            style = "";
        } else {
            coll_text = mpi_collective_names[code];
            style = "style=filled";
        }

        fprintf(out,
            "\tN%02d [label=\"Node %02d\\n%s\\n(code %d)\" shape=ellipse %s "
            "color=\"%s\"]\n",
            bb->index,
            bb->index,
            coll_text,
            code,
            style,
            mpi_collective_colors[code]);
    }

    FOR_EACH_BB_FN(bb, fun)
    {
        edge e;
        edge_iterator ei;
        FOR_EACH_EDGE(e, ei, bb->succs)
        {
            if (e->dest == EXIT_BLOCK_PTR_FOR_FN(fun)) {
                continue;
            }

            fprintf(out, "\tN%02d -> N%02d\n", e->src->index, e->dest->index);
        }
    }

    // Close the main graph
    fprintf(out, "}\n");
}

// Dump a function as a graph, appending `suffix` to the filename.
void cfgviz_dump(const function* const fun, const char* const suffix)
{
    char* target_filename;
    FILE* out;

    target_filename = cfgviz_generate_filename(fun, suffix);

    printf("[GRAPHVIZ] Generating CFG of function %s in file <%s>\n",
        current_function_name(),
        target_filename);

    out = fopen(target_filename, "w");

    cfgviz_internal_dump(fun, out);

    fclose(out);
    free(target_filename);
}
