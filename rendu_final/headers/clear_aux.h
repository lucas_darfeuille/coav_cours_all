#pragma once

// This file provides a function and a `typedef`ed handler to properly
// cleanup the basic blocks in a function after a pass finished its work.
//
// See `clear_all_basic_blocks_in_fn` and `clear_basic_block_handler` for
// more informations.
#include "plugin_headers.h"

// `clear_basic_block_handler` is a function that takes a basic block and clear
// its `bb->aux` value, when necessary.
typedef void (*clear_basic_block_handler)(basic_block bb);

// Calls `handler` on each basic block in the function `fun`, in no particular
// order and ensures `bb->aux` is set to `NULL` after that.
//
// If `handler` is `NULL`, `bb->aux` is set to `NULL` but nothing is done to
// free memory that could have been allocated and be owned by `bb->aux`.
void clear_all_basic_blocks_in_fn(
    const function* const fun, const clear_basic_block_handler handler)
{
    basic_block bb = NULL;
    FOR_ALL_BB_FN(bb, fun)
    {
        if (handler != NULL) {
            // Use the dereferencing style to make it clear `handler` is a
            // parameter and not a function defined somewhere else.
            (*handler)(bb);
        }
        bb->aux = (void*)NULL;
    }
}
