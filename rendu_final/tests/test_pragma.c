/**
 * \file main.c
 * \brief TEST PLUGIN
*/
#include <stdio.h>
#include "../headers/test_pragma.h"

#pragma ProjetCA mpicoll_check mock1
#pragma ProjetCA mpicoll_check mock2
#pragma ProjetCA mpicoll_check mock3,mock5
#pragma ProjetCA mpicoll_check (mock4,mock6)
#pragma ProjetCA mpicoll_check CANARD
#pragma ProjetCA mpicoll_check mock1
#pragma ProjetCA mpicoll_check (coincoin

void mock1() {
	printf("mock1\n");
}

void mock2() {
	printf("mock2\n");
}

void mock3() {
	printf("mock3\n");
}

void mock4() {
	printf("mock4\n");
}
