#include <string.h>
#include <stdio.h>
#include "headers/plugin_headers.h"
#include "headers/bitmap_array.h"
#include "headers/clear_aux.h"
#include "headers/cfgviz.h"
#include "headers/mpi_collectives.h"
#include "headers/queue.h"


/*********************
 * 
 * DEBUT
 * Gestion des PRAGMA
 * 
**********************/

// Vecteur qui servira pour la liste des fonctions instruments
vec<const unsigned char*> pragma_instrumented_functions {};

/******************
 * 
 * TD6 - Question 3
 * 
*******************/
// Ceci est la fonction instrument 
// Elle est appelée lors de la reconnaissance d'un pragma (cf ci-dessous)
// On s'inspire de ce qu'il se fait dans la fonction handle_pragma_target
void pragma_instrument_function(cpp_reader* ARG_UNUSED(dummy)) {
    /*
    * Début question 4
    */
    // Gestion d'erreur, si on est dans une fonction, on le signale
    // Car on ne met pas de pragma ProjetCA dans nos fonctions
    if (cfun) {
        error("pragma ProjetCA option is not allowed inside functions");
        return;
    }
    /*
     * Fin question 4
    */

    // On récupère les informations utiles
    // Le booléen est là pour vérifier si le pragma est bien formé (vis à vis des parenthèses)
    bool closed_parenthese = false;
    tree x;
    enum cpp_ttype token;

    //pragma_lex permet d'avoir le prochain "mot" du pragma
    // Donc on commence par regarder si on a une parenthèse ouvrante
    token = pragma_lex(&x);
    if (CPP_OPEN_PAREN == token) {
        // il faudra refermer la parenthèse
        closed_parenthese = true;
        // On passe à l'élément suivant pour la suite du traitement
        token = pragma_lex(&x);
    }

    /*
    * Début question 6 partie 1
    */
    // On créé un vecteur dans lequel on va ajouter les fonctions à mettre
    // A la fin, si le pragma est bien formé, on ajoutera le tout à la variable globale
    // Sinon, on ne fait rien
    vec<const unsigned char*> functions_to_add {};

    //On définit la variable définira si on doit ajouter ou pas
    bool add_instrumented_to_global_variable = true;
    // Ceci nous permet d'avancer dans le traitement des éléments du pragma
    // On change quand on reconnait un CPP_COMMA (une virgule)
    // On traite alors bien les #pragma ProjetCA function (f1,f2,f3...)
    while (CPP_NAME == token) {
        
        //On commence par récupérer le nom de notre fonction
        const unsigned char* name = x->identifier.id.str;
        // Puis on la met dans la liste de nos fonctions instrumentées
        functions_to_add.safe_push(name);
        /*
        * Fin question 6 partie 1
        */

        token = pragma_lex(&x);
        if (token == CPP_COMMA && !closed_parenthese) {
                error("pragma (name1,name2,etc) is missing an openning parenthese");
                return;
            } 
        
        if (token == CPP_CLOSE_PAREN) {
            break;
        }

        token = pragma_lex(&x);
        // do {
        //     token = pragma_lex(&x);
        // } while (CPP_COMMA == token);
    }

    /*
    * Début question 6 partie 2
    */
    // Quand on arrive ici, on a finit les fonctions, donc il faut clore la parenthèse ouvrante
    // C'est ce qu'on vérifie ici
    if (closed_parenthese && CPP_CLOSE_PAREN != token) {
        //Si on est ici, c'est que notre pragma est malformé, donc on ne doit pas ajouter les fonctions
        add_instrumented_to_global_variable = false;
        error("pragma ProjetCA (name1, name2, etc) is missing a closing parenthese");
        return;
    }

    // Gestion d'erreur
    // Si on continue de reconnaitre des éléments après la fin théorique du pragma
    // Alors il faut prévenir l'utilisateur que ce dernier est mal écrit
    token = pragma_lex(&x);
    if (CPP_EOF != token) {
        //Si on est ici, c'est que notre pragma est malformé, donc on ne doit pas ajouter les fonctions
        add_instrumented_to_global_variable = false;
        error("pragma ProjetCA is badly formed");
        return;
    }
    /*
    * Fin question 6 partie 2
    */

    /*
    * Début question 6 partie 3
    */
    // On vérifie si on doit ajouter les fonctions instrumentés
    // Le cas échéant, on ajoute le tout à la variable globale
    unsigned int i;
    const unsigned char* fun_name;
    if (add_instrumented_to_global_variable) {
        // On prend chaque élément du vecteur et on le push dans la var globale
        FOR_EACH_VEC_ELT(functions_to_add, i, fun_name) {
            // On vérifie que la fonction à ajouter n'est pas dans la var globale
            // Si elle dedans, on fait un warning et on ne l'ajoute pas
            // Mdr avant je me suis compliqué.e la vie, ça c'est simple
            if (pragma_instrumented_functions.contains(fun_name)) {
                //Emission du warning
                fprintf(stderr, "%s: warning: '%s' is duplicated in '#pragma ProjetCA ...'\n", progname, fun_name);
            } 
            else {
            pragma_instrumented_functions.safe_push(fun_name);
            }
        }
    }
    /*
    * Fin question 6 partie 3
    */
}

// Fonction permettant d'enregistrer nos pragma customs
// C'est c_register_pragma qui fait la travail ici. 
// Quand est reconnu un #pragma ProjetCA function, alors on appelle pragma_instrument_function
void register_my_pragma(void *event_data, void *data)
{
    c_register_pragma("ProjetCA", "mpicoll_check", pragma_instrument_function);
    //printf("Nouveau pragma !\n");
}

/*
* Début question 8 partie 1
*/
// Fonction permettant de vérifier si toutes les fonctions instrumentées sont vue
// On est garanti que tout à été traité grâce à l'endroit où on insert le callback
// Si le vecteur est vide on doit faire un warning sur chaque fonction présente dans le vecteur
// Sinon, on ne fait rien, pas de soucis à lever à la compilation
void is_instrumented_function_empty(void *event_data, void *data) {
    if (pragma_instrumented_functions.is_empty()) {
        return;
    }

    unsigned int pos;
    const unsigned char* searcher;
    FOR_EACH_VEC_ELT(pragma_instrumented_functions, pos, searcher)
    {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
        error("%<#pragma ProjetCA%> got argument %<%s%> but the function was never seen, possible missing declaration", searcher);
#pragma GCC diagnostic pop
    }
}
/*
* Fin question 8 partie 1
*/

/*********************
 * 
 * FIN
 * Gestion des PRAGMA
 * 
**********************/




/*********************
 * 
 * DEBUT
 * Gestion des DEADLOCKS
 * 
**********************/

/****************************
 * 
 * Définition des fonctions auxiliaires 
 * On les appellera ensuite en conséquence dans la fonction principale de notre plugin.
 * 
*****************************/

/*****************
 * 
 * TD5 QUESTION 1
 * 
 * ***************/
// Calcule et affiche à l'aide de bitmap la frontière de post dominance
// Et ce pour chaque BB (seulement les réels, pas les virtuels)
void calc_post_dominance_frontiers(
    const function* const fun, bitmap_array& post_dom_frontiers)
{
    basic_block bb = NULL;
    FOR_EACH_BB_FN(bb, fun)
    {
        // >= 2 : si on a un seul successeur on ne peut pas être la frontière
        if (EDGE_COUNT(bb->succs) >= 2) {
            edge e;
            edge_iterator ei;
            FOR_EACH_EDGE(e, ei, bb->succs)
            {
                basic_block runner = e->dest;

                if (runner == EXIT_BLOCK_PTR_FOR_FN(fun))
                    continue;

                // Find the frontier.
                basic_block immediate_post_dominator
                    = get_immediate_dominator(CDI_POST_DOMINATORS, bb);
                while (runner != immediate_post_dominator) {
                    if (!bitmap_set_bit(
                            &post_dom_frontiers[runner->index], bb->index))
                        break;

                    runner
                        = get_immediate_dominator(CDI_POST_DOMINATORS, runner);
                }
            }
        }
    }

    // On clear ensuite
    bb = NULL;
    FOR_EACH_BB_FN(bb, fun)
    {
        bitmap_clear_bit(&post_dom_frontiers[bb->index], bb->index);
    }
}

/*****************
 * 
 * TD5 QUESTION 2
 * 
 * ***************/
// Calcule le CFG', CFG dans lequel on retire les boucles
void calc_new_cfg_without_loops(const function* const fun, bitmap_array& all_preds)
{
    // On utilise une pile pour notre parcours de graphe en largeur
    Queue<basic_block> queue {};
    queue.push(ENTRY_BLOCK_PTR_FOR_FN(fun)->next_bb);

    // Parcours classique : tant qu'on a un noeud dans la pile :
    // On le dépile et on prend les infos intéressantes
    while (!queue.is_empty()) {
        basic_block current_bb = queue.pop();
        bitmap_set_bit(&all_preds[current_bb->index], current_bb->index);

        // On itère sur tous les successeurs qui ne sont *pas* la fin de la fonction
        edge e;
        edge_iterator ei;
        FOR_EACH_EDGE(e, ei, current_bb->succs)
        {
            // Infos sur la destinations (et on s'assure que ce n'est pas la fin de la fonction)
            basic_block dest_bb = e->dest;
            if (dest_bb == EXIT_BLOCK_PTR_FOR_FN(fun)) {
                continue;
            }

            // Le coeur : on vérifie si une destination n'est pas un prédécesseur
            // Auquel cas on aurait une boucle à supprimer
            edge e2;
            edge_iterator ei2;
            bool is_already_pred = false;
            FOR_EACH_EDGE(e2, ei2, current_bb->preds)
            {
                basic_block src_bb = e->src;
                if (bitmap_bit_p(&all_preds[src_bb->index], dest_bb->index)) {
                    is_already_pred = true;
                    break;
                }
            }

            // Si on a déjà marqué une successeur comme predecesseur alors on continue
            if (is_already_pred) {
                continue;
            }

            bitmap_ior_into(
                &all_preds[dest_bb->index], &all_preds[current_bb->index]);
            queue.push(dest_bb);
        }
    }
}

// Fonction auxiliaire pour la suite
// 'true' si le bitmap 'outer' continent le bitmap 'inner', 'false' sinon
bool bitmap_contains(const bitmap outer, const bitmap inner)
{
    static bitmap and_bm = bitmap_alloc(NULL);
    bitmap_and(and_bm, outer, inner);
    return bitmap_equal_p(and_bm, inner);
}

/*****************
 * 
 * TD5 QUESTION 3
 * Partie 1
 * 
 * ***************/
// Le premier bloc aura le rang 0, et le dernier aura le rang max
//
// Implémentation de la formule donnée dans le sujet
// Note : il est important d'appeler cette fonction sur un CFG sans boucle.
void calc_MPI_rank(
    const function* const fun, const bitmap_array& all_preds, uint64_t ranks[])
{
    size_t idx = 0;
    FOR_EACH_BITMAP_ARRAY_ELEM(all_preds, idx) { ranks[idx] = 0; }

    bitmap_head visited;
    bitmap_initialize(&visited, &bitmap_default_obstack);

    Queue<basic_block> queue {};
    queue.push(ENTRY_BLOCK_PTR_FOR_FN(fun)->next_bb);

    while (!queue.is_empty()) {
        basic_block current_bb = queue.pop();
        uint64_t current_rank = ranks[current_bb->index];

        bitmap_set_bit(&visited, current_bb->index);
        if (!bitmap_contains(&visited, &all_preds[current_bb->index])) {
            bitmap_clear_bit(&visited, current_bb->index);
            continue;
        }

        edge e;
        edge_iterator ei;
        FOR_EACH_EDGE(e, ei, current_bb->succs)
        {
            basic_block dest_bb = e->dest;

            // If the destination is the exit or a predecessor, ignore it.
            if (dest_bb == EXIT_BLOCK_PTR_FOR_FN(fun)
                || bitmap_bit_p(&all_preds[current_bb->index], dest_bb->index)) {
                continue;
            }

            queue.push(dest_bb);

            // MPI Collective code of the destination basic block.
            uint64_t dest_old_rank = ranks[dest_bb->index];
            if (current_rank > dest_old_rank) {
                ranks[dest_bb->index] = current_rank;
            }

            if (dest_bb->aux != (void*)LAST_AND_UNUSED_MPI_COLLECTIVE_CODE) {
                ranks[dest_bb->index] += reinterpret_cast<uint64_t>(dest_bb->aux)
                    + static_cast<uint64_t>(LAST_AND_UNUSED_MPI_COLLECTIVE_CODE);
            }
        }
    }
}

/*****************
 * 
 * TD5 QUESTION 3
 * Partie 2
 * 
 * ***************/
// Permet de noter les ranks associés aux BB (node) qui les contiennents (et les prédecesseurs)
// Le tout, pour chaque groupe (de BB de même rang) 
//
// MANDATORY : les rangs doivent avoir été remplis avec calc_MPI_rank au préalable
void fill_node_sets_and_rank_set_and_all_preds_by_rank(const function* const fun,
    const bitmap_array& all_preds,
    const uint64_t ranks[],
    bitmap_array& node_sets_by_rank,
    bitmap_head& rank_set,
    bitmap_array& all_preds_by_rank)
{
    // Filling all the relevant bitmaps for each node.
    basic_block bb = NULL;
    FOR_EACH_BB_FN(bb, fun)
    {
        const uint64_t rank = ranks[bb->index];
        bitmap_set_bit(&rank_set, rank);
        // Uncomment when you want to only include the basic blocks with MPI
        // calls in them.
        if (bb->aux == (void*)LAST_AND_UNUSED_MPI_COLLECTIVE_CODE) {
            continue;
        }
        bitmap_set_bit(&node_sets_by_rank[rank], bb->index);
        bitmap_ior_into(&all_preds_by_rank[rank], &all_preds[bb->index]);
    }

    // Ensuring a node is not marked as a predecessor of itself.
    bb = NULL;
    FOR_EACH_BB_FN(bb, fun)
    {
        const uint64_t rank = ranks[bb->index];
        bitmap_clear_bit(&all_preds_by_rank[rank], bb->index);
    }
}

/*****************
 * 
 * TD5 QUESTION 4
 * 
 * ***************/
// Comme avant (Q1) mais pour un ensemble
//
//MANDATORY : les rangs doivent avoir été remplis avec calc_MPI_rank au préalable
void calc_post_dominance_frontier_for_set(const function* const fun,
    const bitmap_array& all_preds_by_rank,
    const bitmap_head& rank_set,
    const bitmap_array& node_sets_by_rank,
    bitmap_array& groups_post_dom)
{
    size_t rank = 0;
    // Pour chaque rang, on complète le tableau des noeuds post dominés par son rang
    FOR_EACH_BITMAP_ARRAY_ELEM(node_sets_by_rank, rank)
    {
        // On ignore les ranks qui n'ont aucun noeud
        if (!bitmap_bit_p((bitmap)&rank_set, rank)) {
            continue;
        }

        Queue<basic_block> queue {};

        basic_block node = NULL;
        FOR_EACH_BB_FN(node, fun)
        {
            if (!bitmap_bit_p(&node_sets_by_rank[rank], node->index)) {
                continue;
            }

            if (EDGE_COUNT(node->preds) == 1) {
                const basic_block pred = EDGE_PRED(node, 0)->src;
                // Assure que le prédecesseur c'est pas l'entrée de la fonction
                if (pred != ENTRY_BLOCK_PTR_FOR_FN(fun)
                    // Assure que le prédecesseur n'est pas à l'origine d'une boucle
                    && bitmap_bit_p(&all_preds_by_rank[rank], pred->index)
                    && !bitmap_bit_p(&node_sets_by_rank[rank], pred->index)) {
                    // Dans ce cas, on doit empiler notre noeud
                    queue.push(pred);
                    continue;
                }
            }
        }
        // A ce stade, la pile n'a que des noeuds post-dominé par notre rang

        node = NULL;
        FOR_EACH_BB_FN(node, fun)
        {
            if (!bitmap_bit_p(&node_sets_by_rank[rank], node->index)) {
                continue;
            }
            if (EDGE_COUNT(node->preds) == 1) {
                continue;
            }

            edge e_preds = NULL;
            edge_iterator ei_preds;
            FOR_EACH_EDGE(e_preds, ei_preds, node->preds)
            {
                const basic_block pred = e_preds->src;

                // Ignore le BB d'entrée de la fonction
                if (pred == ENTRY_BLOCK_PTR_FOR_FN(fun)) {
                    continue;
                }
                // Et le prédécesseur s'il a le même rang
                if (bitmap_bit_p(&node_sets_by_rank[rank], pred->index)) {
                    continue;
                }
                // Enfin, les loops sont aussi ignorés
                if (!bitmap_bit_p(&all_preds_by_rank[rank], pred->index)) {
                    continue;
                }
                queue.push(pred);
            }
        }
        // À ce stade, la pile n'a que des prédecesseurs direct du rank courant

        while (!queue.is_empty()) {
            const basic_block pred = queue.pop();

            edge e_succs = NULL;
            edge_iterator ei_succs {};
            bool all_post_dom_or_in_group = true;
            FOR_EACH_EDGE(e_succs, ei_succs, pred->succs)
            {
                const basic_block succ = e_succs->dest;

                if (succ == EXIT_BLOCK_PTR_FOR_FN(fun)) {
                    all_post_dom_or_in_group = false;
                    break;
                }

                // Si le successeur a le même rang que 'node', ou qu'il est post dominé par
                // le groupe de rang 'rank', on continue
                if (bitmap_bit_p(&groups_post_dom[rank], succ->index)
                    || bitmap_bit_p(&node_sets_by_rank[rank], succ->index)) {
                    continue;
                }

                // 'succ' n'est pas post dominé par 'rank' et n'as pas le même rang que 'node'
                // Donc il n'est pas dans le groupe : on break le tour de boucle
                all_post_dom_or_in_group = false;
                break;
            }

            // Si tous les successeurs ont le même rang que 'node ou sont post-dominés par 'rank'
            // Alors 'pred' est post dominé aussi par 'rank'
            if (all_post_dom_or_in_group) {
                bitmap_set_bit(&groups_post_dom[rank], pred->index);

                edge e_pred = NULL;
                edge_iterator ei_pred {};
                FOR_EACH_EDGE(e_pred, ei_pred, pred->preds)
                {
                    const basic_block src = e_pred->src;
                    if (src != ENTRY_BLOCK_PTR_FOR_FN(fun)
                        && bitmap_bit_p(&all_preds_by_rank[rank], src->index)) {
                        queue.push(src);
                    }
                }
            }
        }
    }
}


/*****************
 * 
 * TD5 QUESTION 7
 * 
 * ***************/
// Note pour toi Kasbone :
// A partir de là j'ai vraiment repris un truc qui existait déjà parce que fuck 
// Au moins ça fonctionne x))) 
void iterated_pdf(
    const function* const fun,
    const bitmap_array& all_preds_by_rank,
    const bitmap_head& rank_set,
    const bitmap_array& node_sets_by_rank,
    const bitmap_array& post_dom_frontiers,
    const bitmap_array& groups_post_dom,
    bitmap_array& groups_post_dom_frontier)
{
    size_t rank = 0;
    // For each rank, complete the array of nodes that are post-dominated
    // by this rank.
    FOR_EACH_BITMAP_ARRAY_ELEM(node_sets_by_rank, rank)
    {
        // Ignore ranks that do not contains any node.
        if (!bitmap_bit_p((bitmap)&rank_set, rank)) {
            continue;
        }

        // First gather the post-dominance frontiers of each node into the
        // post-dominance frontier for the group.
        basic_block node = NULL;
        FOR_EACH_BB_FN(node, fun)
        {
            bitmap_ior_into(&groups_post_dom_frontier[rank],
                &post_dom_frontiers[node->index]);
        }

        node = NULL;
        FOR_EACH_BB_FN(node, fun)
        {
            if (!bitmap_bit_p(&all_preds_by_rank[rank], node->index)
                || bitmap_bit_p(&groups_post_dom[rank], node->index)
                || bitmap_bit_p(&node_sets_by_rank[rank], node->index)) {
                bitmap_clear_bit(&groups_post_dom_frontier[rank], node->index);
            }
        }
    }
}

// Fills `to_warn_blocks` with the basic block indexes that could be problematic
// when running the program with respect to MPI collective calls.
void fill_bitmap_to_warn_blocks(
    const bitmap_array& groups_post_dom_frontier, bitmap_head& to_warn_blocks)
{
    size_t rank = 0;
    FOR_EACH_BITMAP_ARRAY_ELEM(groups_post_dom_frontier, rank)
    {
        bitmap_ior_into(
            (bitmap)&to_warn_blocks, &groups_post_dom_frontier[rank]);
    }
}

/********************
 * 
 * MAIN ENTRY
 * OF THE PLUGIN
 * FOR DEADLOCKS
 * 
 *******************/

// On run ici toutes nos fonctions auxiliaires
// Le tout en prenant garde à initialiser les bonnes données
void run_all_helpers(function* fun)
{
    calculate_dominance_info(CDI_POST_DOMINATORS);

    const size_t basic_block_count = (size_t)last_basic_block_for_fn(fun);

    // Q1.
    bitmap_array post_dom_frontiers { basic_block_count };
    //Appel à notre fonction auxiliaire :
    calc_post_dominance_frontiers(fun, post_dom_frontiers);
    // printf("Q1. Post dominance frontier for a node.\n");
    basic_block bb = NULL;
    // FOR_EACH_BB_FN(bb, fun)
    // {
    //     printf("\tBB %02d: ", bb->index);
    //     bitmap_print(stdout, &post_dom_frontiers[bb->index], "", "\n");
    // }
    // printf("Done\n");

    // Q2.
    bitmap_array all_preds { basic_block_count };
    //Appel à notre fonction auxiliaire :
    calc_new_cfg_without_loops(fun, all_preds);
    // printf("Q2. CFG'.\n");
    // bb = NULL;
    // FOR_EACH_BB_FN(bb, fun)
    // {
    //     printf("\tBB %02d: ", bb->index);
    //     bitmap_print(stdout, &all_preds[bb->index], "", "\n");
    // }
    // printf("Done\n");

    // Q3.
    uint64_t ranks[basic_block_count];
    // Tout ce passe bien cas all_preds est le bitmap représentant notre CFG'
    // Qui est donc sans loops
    calc_MPI_rank(fun, all_preds, ranks);
    // printf("Q3. Rank.\n");
    // bb = NULL;
    // FOR_EACH_BB_FN(bb, fun)
    // {
    //     printf("\tBB %02d: rank %02ld\n", bb->index, ranks[bb->index]);
    // }
    // printf("Done\n");

    // Filling informations about the ranks.
    const uint64_t rank_count = ranks[basic_block_count - 1] + 1;
    bitmap_array all_preds_by_rank { (size_t)rank_count };
    bitmap_array node_sets_by_rank { (size_t)rank_count };
    bitmap_head rank_set;
    bitmap_initialize(&rank_set, &bitmap_default_obstack);
    // On a bien calculé les rangs avant, plus qu'à remplir les infos associées :
    fill_node_sets_and_rank_set_and_all_preds_by_rank(
        fun, all_preds, ranks, node_sets_by_rank, rank_set, all_preds_by_rank);
    // printf("Informations about the ranks.\n");
    // bitmap_print(stdout, &rank_set, "\trank set: ", "\n");
    //size_t i = 0;
    // FOR_EACH_BITMAP_ARRAY_ELEM(node_sets_by_rank, i)
    // {
    //     if (!bitmap_bit_p(&rank_set, i)) {
    //         continue;
    //     }
    //     printf("\trank [%02ld]\n", i);
    //     bitmap_print(stdout, &node_sets_by_rank[i], "\t\tnodes: ", "\n");
    //     bitmap_print(stdout, &all_preds_by_rank[i], "\t\tpreds: ", "\n");
    // }
    // printf("Done\n");

    // Q4.
    bitmap_array groups_post_dom { (size_t)rank_count };
    // Appel à notre fonction auxiliaire, tout est toujours bien set-up
    calc_post_dominance_frontier_for_set(
        fun, all_preds_by_rank, rank_set, node_sets_by_rank, groups_post_dom);
    // printf("Q4. Post dominance for a group.\n");
    // i = 0;
    // FOR_EACH_BITMAP_ARRAY_ELEM(node_sets_by_rank, i)
    // {
    //     if (!bitmap_bit_p(&rank_set, i)) {
    //         continue;
    //     }
    //     printf("\trank [%02ld] ", i);
    //     bitmap_print(stdout, &groups_post_dom[i], "", "\n");
    // }
    // printf("Done\n");

    // Q5 et Q7 sont similaires, il n'y a donc ici que la Q7
    bitmap_array groups_post_dom_frontier { (size_t)rank_count };
    iterated_pdf(fun,
        all_preds_by_rank,
        rank_set,
        node_sets_by_rank,
        post_dom_frontiers,
        groups_post_dom,
        groups_post_dom_frontier);
    // printf("Q5. Post dominance frontier for a group.\n");
    // i = 0;
    // FOR_EACH_BITMAP_ARRAY_ELEM(node_sets_by_rank, i)
    // {
    //     if (!bitmap_bit_p(&rank_set, i)) {
    //         continue;
    //     }
    //     printf("\trank [%02ld] ", i);
    //     bitmap_print(stdout, &groups_post_dom_frontier[i], "", "\n");
    // }
    // printf("Done\n");

    // Q6.
    // printf("Q6. First warnings.\n");
    // i = 0;
    // FOR_EACH_BITMAP_ARRAY_ELEM(node_sets_by_rank, i)
    // {
    //     if (!bitmap_bit_p(&rank_set, i)) {
    //         continue;
    //     }
    //     printf("\trank [%02ld] ", i);

    //     bitmap_print(stdout, &node_sets_by_rank[i], "(Nodes: ", "): ");

    //     if (bitmap_count_bits(&groups_post_dom_frontier[i]) > 0) {
    //         printf("ISSUE WITH MPI CALLS\n");
    //     } else

    //     {
    //         printf("no issue with mpi calls\n");
    //     }
    // }
    // printf("Done\n");

    // I think I did Q7 at the same time as Q5 and I don't really understand
    // what an iterated PDF is anyway so for now let's do Q8.

    // Q8.
    //printf("Q8. Better warnings.\n");
    bitmap_head to_warn_blocks;
    bitmap_initialize(&to_warn_blocks, &bitmap_default_obstack);
    fill_bitmap_to_warn_blocks(groups_post_dom_frontier, to_warn_blocks);
    bb = NULL;
    FOR_EACH_BB_FN(bb, fun)
    {
        // Ignores basic blocks that are not to be warned about.
        if (!bitmap_bit_p(&to_warn_blocks, bb->index)) {
            continue;
        }

        const gimple_stmt_iterator gsi = gsi_last_bb(bb);
        const gimple* stmt = gsi_stmt(gsi);
        const location_t loc = gimple_location(stmt);

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
        warning_at(loc,
            OPT_fplugin_,
            "Basic block %<%d%> causes a possibly invalid split in the MPI "
            "collectives call sequence",
            bb->index);
#pragma GCC diagnostic pop
    }
    //printf("Done\n");

    // Freeing things.
    free_dominance_info(CDI_POST_DOMINATORS);
}


/*****************************************************************/


/*********************
 * 
 * Gestion de la PASS
 * 
**********************/
const pass_data my_pass_data = {
    .type = GIMPLE_PASS,
    .name = "TD6 Q5",
    .optinfo_flags = OPTGROUP_NONE,
    .tv_id = TV_OPTIMIZE,
    .properties_required = 0,
    .properties_provided = 0,
    .properties_destroyed = 0,
    .todo_flags_start = 0,
    .todo_flags_finish = 0,
};

class my_pass : public gimple_opt_pass {
public:
    my_pass(gcc::context* ctxt)
        : gimple_opt_pass(my_pass_data, ctxt)
    {
    }

    my_pass* clone() { return new my_pass(g); }

    /*
    * Début question 8 partie 2
    */
    bool gate(function* fun)
    {
        return true;
    /*
    * Fin question 8 partie 2
    */
    }

    unsigned int execute(function* fun)
    {
        /*
         * 
         * Gestion deadlocks
         * 
        */
        // Splitting to have only one MPI call in each basic block.
        split_on_mpi_collectives(fun);
        set_aux_to_mpi_code(fun);

        run_all_helpers(fun);

        // Final cleanup.
        clear_all_basic_blocks_in_fn(fun, NULL);

        /*
         * 
         * Gestion pragma
         * 
        */
       const unsigned char* fname = (const unsigned char*)function_name(fun);

        unsigned int pos;
        const unsigned char* searcher;
        FOR_EACH_VEC_ELT_REVERSE(pragma_instrumented_functions, pos, searcher)
        {
            // Unsafe comparison
            if (fname == searcher || strcmp((const char*)fname, (const char*)searcher) == 0) {
                pragma_instrumented_functions.ordered_remove(pos);
                break;
            }
        }

        return 0;
    }
};

int plugin_is_GPL_compatible = 1;

int plugin_init(struct plugin_name_args* plugin_info,
    struct plugin_gcc_version* version)
{
    if (!plugin_default_version_check(version, &gcc_version))
        return 1;

    my_pass p(g);

    struct register_pass_info my_pass_info = {
        .pass = &p,
        .reference_pass_name = "cfg",
        .ref_pass_instance_number = 0,
        .pos_op = PASS_POS_INSERT_AFTER,
    };

    //C'est ici que nos fonctions sont appelées
    register_callback(plugin_info->base_name, PLUGIN_PASS_MANAGER_SETUP, NULL, &my_pass_info);
    register_callback(plugin_info->base_name, PLUGIN_PRAGMAS, register_my_pragma, NULL);
    register_callback(plugin_info->base_name, PLUGIN_FINISH, is_instrumented_function_empty, NULL);

    return 0;
}
