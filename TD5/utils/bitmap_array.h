#pragma once

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "plugin_headers.h"

// An array of bitmap of known and fixed size.
class bitmap_array {
public:
    // Initialize a new array with the given length.
    //
    // Length must be non-zero.
    bitmap_array(const size_t ll)
        : ptr(XNEWVEC(bitmap, ll))
        , len(ll)
    {
        for (size_t i = 0; i < this->len; i += 1)
            this->ptr[i] = bitmap_alloc(NULL);
    }

    ~bitmap_array()
    {
        for (size_t i = 0; i < this->len; i += 1)
            bitmap_release(this->ptr[i]);
        free(this->ptr);
    }

    // Accesses the element at index `idx`. If the index is out of bound,
    // the program will panic and exit with code 1.
    bitmap_head& operator[](const size_t idx) const
    {
        if (idx >= this->length()) {
            fprintf(stderr,
                "ERROR: bitmap_array: idx(%ld) >= this->length(%ld)\n",
                idx,
                this->length());
            exit(1);
        }

        return *this->ptr[idx];
    }

    // Number of bitmap_head in the array.
    size_t length() const { return this->len; }

private:
    // Pointer to the array head.
    bitmap* ptr;
    // Size of the array.
    size_t len;
};

#ifndef FOR_EACH_BITMAP_ARRAY_ELEM
// Helper macro to iterate over the indexes of an array.
#define FOR_EACH_BITMAP_ARRAY_ELEM(ARR, INDEX)                                  \
    for ((INDEX) = 0; (INDEX) < (ARR).length(); (INDEX)++)
#endif // FOR_EACH_BITMAP_ARRAY_ELEM
