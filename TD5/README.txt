Pour compiler le plugin sur le cluster : 

$ source /home/patrick.carribault/setenv.sh
$ g++_1220 -I`gcc_1220 -print-file-name=plugin`/include -g -Wall -fno-rtti -shared -fPIC -o libplugin_tp_5.so tp_5_only.cpp

Pour lancer un test avec un fichier test/test2.c : 

$ mpicc test/test2.c -c -fplugin=libplugin_tp_5.so


NOTE :
Le `test1_pass.c` est un fichier de test où tout fonctionne (pas de deadlock)
Les fichiers test2.c et test3.c sont des fichiers avec des soucis.
